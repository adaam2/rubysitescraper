require "rubygems"
require "google/api_client"
require "google_drive"

# Authorizes with OAuth and gets an access token.
client = Google::APIClient.new
auth = client.authorization
auth.client_id = "" # reminder: register a new API app at https://console.developers.google.com
auth.client_secret = "" # client secret here
auth.scope =
    "https://www.googleapis.com/auth/drive " +
    "https://spreadsheets.google.com/feeds/"
auth.redirect_uri = "urn:ietf:wg:oauth:2.0:oob"
print("1. Open this page:\n%s\n\n" % auth.authorization_uri)
print("2. Enter the authorization code shown in the page: ")
auth.code = $stdin.gets.chomp
auth.fetch_access_token!
access_token = auth.access_token

domain = "" # your domain name here. Or mod this so it receives console input (later)

# Creates a session.
session = GoogleDrive.login_with_oauth(access_token)

# Uploads a local file.
file = session.upload_from_file("spreadsheet-template.xlsx", domain, :convert => true)


key = file.key

print("3. The document has been created. The url is https://docs.google.com/spreadsheet/ccc?key=" + key + "\n")

# first worksheet
ws = file.worksheets[0];

# establish result counter and other counters
resultcounter = 1
jumpcounter = 100

domainprefix = "https://www.google.co.uk/search"

# Make use of Google Sheets magic!
ws[resultcounter,1] = %Q[=importXml("#{domainprefix}?start=#{resultcounter.to_s()}&num=100&as_sitesearch=#{domain}","//cite")]

ws.save
ws.reload

# Enumerate the rows of the current Google Sheet, pulling in the matching domain results from Google Search.
# If the row 100 rows down from the current marker doesn't have any content, then we can stop the while loop because there are no more google search results for this domain!
while ws[resultcounter + jumpcounter - 1, 1].length > 0
	ws[resultcounter + jumpcounter, 1] = %Q[=importXml("#{domainprefix}?start=#{resultcounter.to_s()}&num=100&as_sitesearch=#{domain}","//cite")]
	resultcounter = resultcounter + jumpcounter
	ws.save
	ws.reload
end
